module git.championtek.com.tw/go/sego

go 1.12

require (
	github.com/adamzy/cedar-go v0.0.0-20170805034717-80a9c64b256d
	github.com/issue9/assert v1.3.3
)
