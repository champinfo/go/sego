package sego

import (
	"bytes"
	"fmt"
	"io"
	"os"
)

//SegmentsToString 輸出分詞結果為字串符
//兩種輸出模式，主要參考字典單詞索引
//普通模式(searchMode=false) 輸出一個分詞 "台灣小吃/ns"
//搜索模式(searchMode=true) 輸出普通模式再細分，"台灣/nz 小吃/nz 台灣小吃/ns"
func SegmentsToString(segs []Segment, searchMode bool) (output string) {
	if searchMode {
		for _, seg := range segs {
			output += tokenToString(seg.token)
		}
	} else {
		for _, seg := range segs {
			output += fmt.Sprintf("%s/%s ", textSliceToString(seg.token.text), seg.token.pos)
		}
	}
	return
}

//SegmentsToSlice 輸出結果到一個字串符slice
//兩種輸出模式
//普通模式(searchMode=false) 輸出一個分詞 "[台灣小吃]"
//搜索模式(searchMode=true) 輸出普通模式再細分 "[台灣 小吃 台灣小吃]"
func SegmentsToSlice(segs []Segment, searchMode bool) (output []string) {
	if searchMode {
		for _, seg := range segs {
			output = append(output, tokenToSlice(seg.token)...)
		}
	} else {
		for _, seg := range segs {
			output = append(output, seg.token.Text())
		}
	}
	return
}

func tokenToString(token *Token) (output string) {
	hasOnlyTerminalToken := true
	for _, s := range token.segments {
		if len(s.token.segments) > 1 {
			hasOnlyTerminalToken = false
		}
	}
	if !hasOnlyTerminalToken {
		for _, s := range token.segments {
			if s != nil {
				output += tokenToString(s.token) //繼續切分，recursively
			}
		}
	}
	output += fmt.Sprintf("%s/%s ", textSliceToString(token.text), token.pos)
	return
}

func tokenToSlice(token *Token) (output []string) {
	hasOnlyTerminalToken := true
	for _, s := range token.segments {
		if len(s.token.segments) > 1 {
			hasOnlyTerminalToken = false
		}
	}

	if !hasOnlyTerminalToken {
		for _, s := range token.segments {
			output = append(output, tokenToSlice(s.token)...)
		}
	}
	output = append(output, textSliceToString(token.text))
	return output
}

//將多個字元串接成一個字符輸出
func textSliceToString(text []Text) string {
	return Join(text)
}

//Join concat text
func Join(a []Text) string {
	switch len(a) {
	case 0:
		return ""
	case 1:
		return string(a[0])
	case 2:
		//Special case for common small values
		//Remove if golang.org/6714 si fixed
		return string(a[0]) + string(a[1])
	case 3: //因為一開始我們把字切成3個bytes
		//Special case for common small values
		//Remove if golang.org/6714 is fixed
		return string(a[0]) + string(a[1]) + string(a[2])
	}

	n := 0
	for i := 0; i < len(a); i++ {
		n += len(a[i]) //計算a陣列內字元的總長度
	}

	b := make([]byte, n) //建立一個長度為n byte array
	bp := copy(b, a[0])
	for _, s := range a[1:] {
		bp += copy(b[bp:], s)
	}
	return string(b)
}

func textSliceByteLength(text []Text) (length int) {
	for _, word := range text {
		length += len(word)
	}
	return
}

func textSliceToBytes(text []Text) []byte {
	var buf bytes.Buffer
	for _, word := range text {
		buf.Write(word)
	}
	return buf.Bytes()
}

func createFile(path string) {
	// detect if file exists
	var _, err = os.Stat(path)

	// create file if not exists
	if os.IsNotExist(err) {
		var file, err = os.Create(path)
		if err != nil {
			return
		}
		defer file.Close()
	}

	fmt.Println("==> done creating file", path)
}

func writeFile(path string) {
	// open file using READ & WRITE permission
	var file, err = os.OpenFile(path, os.O_RDWR, 0644)
	if err != nil {
		return
	}
	defer file.Close()

	// write some text line-by-line to file
	_, err = file.WriteString("halo\n")
	if err != nil {
		return
	}
	_, err = file.WriteString("mari belajar golang\n")
	if err != nil {
		return
	}

	// save changes
	err = file.Sync()
	if err != nil {
		return
	}

	fmt.Println("==> done writing to file")
}

func readFile(path string) {
	// re-open file
	var file, err = os.OpenFile(path, os.O_RDWR, 0644)
	if err != nil {
		return
	}
	defer file.Close()

	// read file, line by line
	var text = make([]byte, 1024)
	for {
		_, err = file.Read(text)

		// break if finally arrived at end of file
		if err == io.EOF {
			break
		}

		// break if error occured
		if err != nil && err != io.EOF {
			fmt.Println(err.Error())
			break
		}
	}

	fmt.Println("==> done reading from file")
	fmt.Println(string(text))
}

func deleteFile(path string) error {
	// delete file
	if err := os.Remove(path); err != nil {
		return err
	}

	fmt.Println("==> done deleting file")
	return nil
}
