package sego

// 字串類型:
//	1. 單字，比如“台"或是"灣"，英文的一個字元是一個詞
//	2. 單詞，比如“台灣"或是"人口"
//	3. 文字，比如"台灣有二千三百萬人口"

type DictItem struct {
	Text     string
	FreqText string
	Pos      string
}

//Text define type
type Text []byte

//Token 定義單詞架構
type Token struct {
	//單詞的字串，實際上是個字元數組
	text []Text

	//單詞在詞典中的詞頻
	frequency int

	// log2(totalFrequency/frequency) = log2(1/p(單詞))
	// 用作動態規劃中該分詞的路徑長度，求prod(p(分詞))的最大值 = sum(distance(分詞))的最小值
	// 當frequency一樣時，distance也會一樣
	distance float32

	//詞性標註
	pos string

	// 該單詞進一步分詞劃分
	segments []*Segment
}

//Text 取得單詞文本
func (token *Token) Text() string {
	return textSliceToString(token.text)
}

//Frequency 取得單詞在詞典中的詞頻
func (token *Token) Frequency() int {
	return token.frequency
}

//Pos 取得單詞的詞性標注
func (token *Token) Pos() string {
	return token.pos
}

//Segments 單詞進一步劃分，子單詞也可以進一步有子單詞，形成一個樹狀結構; 主要用於搜索引擎對一段全文進行全文檢索
func (token *Token) Segments() []*Segment {
	return token.segments
}

//TextEquals 比較傳入字串是否跟token單詞一致
func (token *Token) TextEquals(string string) bool {
	tokenLen := 0
	for _, t := range token.text {
		tokenLen += len(t)
	}
	// 先比對長度，待確認長度一致時再繼續往下執行
	if tokenLen != len(string) {
		return false
	}

	byteStr := []byte(string)
	index := 0

	for i := 0; i < len(token.text); i++ {
		textArray := []byte(token.text[i])
		for j := 0; j < len(textArray); j++ {
			if textArray[j] != byteStr[index] {
				index = index + 1
				return false
			}
			index = index + 1
		}
	}
	// 輸入字串與token單詞一樣
	return true
}
