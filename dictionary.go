package sego

import (
	"fmt"
	"github.com/adamzy/cedar-go"
	"log"
)

//Dictionary define structure to implement prefix tree(前綴樹)
//一個分詞可能出現在leaf or non-leaf node
type Dictionary struct {
	trie           *cedar.Cedar //implements double-array trie
	maxTokenLength int          //詞典中最長的分詞
	tokens         []Token      //詞典中所有的分詞
	totalFrequency int64        //詞典中所有分詞的頻率總和
}

//NewDictionary create new dictionary
func NewDictionary() *Dictionary {
	return &Dictionary{trie: cedar.New()}
}

//MaxTokenLength get max token length
func (dict *Dictionary) MaxTokenLength() int {
	return dict.maxTokenLength
}

//NumTokens get number of tokens array
func (dict *Dictionary) NumTokens() int {
	return len(dict.tokens)
}

//TotalFrequency get total frequency value
func (dict *Dictionary) TotalFrequency() int64 {
	return dict.totalFrequency
}

//PrintIDKeyValue a helper function to print the id-key-value triple given trie node id
func (dict *Dictionary) GetIDKeyValue(id int) int {
	// the key of node `id`.
	key, _ := dict.trie.Key(id)
	// the value of node `id`.
	value, _ := dict.trie.Value(id)
	fmt.Printf("%d\t%s:%v\n", id, key, value)

	return value
}

//addToken add token into dictionary with the autoincrement
func (dict *Dictionary) addToken(token Token) {
	bytes := textSliceToBytes(token.text)
	_, err := dict.trie.Get(bytes) //先確認dict.trie是否有值，輸入key後取得value
	if err == nil {                //err為空表示有取到值~~~~
		return
	}

	//Insert(key, value) => Insert(token bytes, token個數)，token個數為遞增
	if err := dict.trie.Insert(bytes, dict.NumTokens()); err != nil {
		log.Fatalln("insert token error: ", err)
	}

	//log.Printf("insert key:value %v:%v....", bytes, dict.NumTokens())
	//fmt.Println("\nPrefixMatch\nid\tkey:value")
	//for _, id := range dict.trie.PrefixMatch(bytes, 0) {//由root=0往下找
	//	dict.GetIDKeyValue(id)
	//}

	dict.tokens = append(dict.tokens, token)
	dict.totalFrequency += int64(token.frequency) //紀錄total frequency
	if len(token.text) > dict.maxTokenLength {
		dict.maxTokenLength = len(token.text) //紀錄最長token
	}
}

//在字典中查找和字元祖words可以前綴匹配的所有分詞
//返回找到的分詞數，numOfTokens
func (dict *Dictionary) lookupTokens(words []Text, tokens []*Token) (numOfTokens int) {
	var id, value int
	var err error
	for _, word := range words {
		// jump to the node first, then search the continue words
		id, err = dict.trie.Jump(word, id)
		//dict.GetIDKeyValue(id)
		if err != nil {
			break
		}
		value, err = dict.trie.Value(id) // the value we get is dict.NumTokens
		if err == nil {
			tokens[numOfTokens] = &dict.tokens[value]
			numOfTokens++
		}
	}
	return
}
