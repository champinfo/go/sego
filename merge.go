package sego

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

type Item struct {
	//單詞的字串，實際上是個字元數組
	text string
	//單詞在詞典中的詞頻
	frequency string
	//詞性標註
	pos string
}

type Dictionaries struct {
	originMap map[string]*Item

	compareMap map[string]*Item

	mergeMap map[string]*Item
}

func NewDictionaries() *Dictionaries {
	return &Dictionaries{originMap: make(map[string]*Item, 0), compareMap: make(map[string]*Item, 0), mergeMap: make(map[string]*Item, 0)}
}

func (dict *Dictionaries) MergeDictionary(origin string, compare string, merge string) error {
	originFile, err := os.Open(origin)
	if err != nil {
		log.Println("open origin file has error: ", err)
		return err
	}

	defer originFile.Close()

	reader := bufio.NewReader(originFile)
	var text string
	var freqText string
	var pos string
	var count int

	//create origin dictionary
	for {
		size, err := fmt.Fscanln(reader, &text, &freqText, &pos)
		if size == 0 { // 沒有單詞存在
			break
		}

		if err != nil {
			log.Println("reading original dictionary has error: ", err)
			return err
		}
		count++

		token := Item{text: text, frequency: freqText, pos: pos}

		if item, ok := dict.originMap[text]; ok {
			log.Printf("%v exist in originmap\nold : %v/%v/%v\nnew : %v/%v/%v", text, item.text, item.frequency, item.pos, text, freqText, pos)
		}

		dict.originMap[text] = &token
	}

	log.Println("origin items are ", len(dict.originMap))
	log.Println("origin items count ", count)

	compareFile, err := os.Open(compare)
	if err != nil {
		log.Println("open compare file has error: ", err)
		return err
	}
	defer compareFile.Close()

	reader = bufio.NewReader(compareFile)

	for {
		size, err := fmt.Fscanln(reader, &text)
		if size == 0 {
			break
		}

		if err != nil {
			log.Println("reading compared dictionary has error: ", err)
			return err
		}

		if _, ok := dict.originMap[text]; !ok {
			//freqText = value.frequency
			//pos = value.pos
			//
			//token := Item{text:text, frequency:freqText, pos:pos}
			//
			////dict.compareMap[text] = &token
			//dict.mergeMap[text] = &token
			freqText = "100"
			pos = "n"

			token := Item{text: text, frequency: freqText, pos: pos}
			dict.originMap[text] = &token
		} //else {//the item of the compared dictionary does not exist in origin dictionary
		//	freqText = "100"
		//	pos = "x"
		//
		//	token := Item{text:text, frequency:freqText, pos:pos}
		//	dict.mergeMap[text] = &token
		//}
	}
	log.Println("compare items also in origin items are ", len(dict.compareMap))
	log.Printf("merge items are %d, should be equal to comapred items %d", len(dict.mergeMap), len(dict.compareMap))

	if err := os.Remove(merge); err != nil {
		log.Println("delete file failed ", err)
	}

	//write the data into merge dictionary
	if _, err := os.Stat(merge); os.IsNotExist(err) {
		var file, err = os.Create(merge)
		if err != nil {
			log.Println("make all path and file has error: ", err)
		}
		defer file.Close()

		mergerFile, err := os.OpenFile(merge, os.O_RDWR, 0644)
		if err != nil {
			log.Println("open merge file has error: ", err)
			return err
		}
		defer mergerFile.Close()

		for _, item := range dict.originMap {
			line := fmt.Sprintf("%s %s  %s\n", item.text, item.frequency, item.pos)
			_, err := mergerFile.WriteString(line)
			if err != nil {
				log.Println("writing into merge file has error: ", err)
			}
		}
		if err := mergerFile.Sync(); err != nil {
			log.Println("sync file error ", err)
		}
	}
	return nil
}
