package sego

//Segment 文本中一個分詞
type Segment struct {
	//單詞在文本中的起始位置
	start int

	//單詞在文本中結束位置(不包含該位置)
	end int

	//單詞信息
	token *Token
}

//Start 取得單詞在文本中的起始位置
func (s *Segment) Start() int {
	return s.start
}

//End 取得單詞在文本中的結束位置
func (s *Segment) End() int {
	return s.end
}

//Token 取得單詞信息
func (s *Segment) Token() *Token {
	return s.token
}
