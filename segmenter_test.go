package sego

import (
	"fmt"
	"testing"
)

var (
	prodSeg = Segmenter{}
)

func TestSplit(t *testing.T) {
	expect(t, "環/台/旅/行/吃/台/灣/小/吃/", bytesToString(splitTextToWords([]byte("環台旅行吃台灣小吃"))))

	expect(t, "github/ /is/ /a/ /web/-/based/ /hosting/ /service/,/ /for/ /software/ /development/ /projects/./", bytesToString(splitTextToWords([]byte("GitHub is a web-based hosting service, for software development projects."))))

	expect(t, "こ/ん/に/ち/は/", bytesToString(splitTextToWords([]byte("こんにちは"))))

	expect(t, "안/녕/하/세/요/", bytesToString(splitTextToWords([]byte("안녕하세요"))))

	expect(t, "Я/ /тоже/ /рада/ /Вас/ /видеть/", bytesToString(splitTextToWords([]byte("Я тоже рада Вас видеть"))))

	expect(t, "¿/cómo/ /van/ /las/ /cosas/", bytesToString(splitTextToWords([]byte("¿Cómo van las cosas"))))

	expect(t, "wie/ /geht/ /es/ /ihnen/", bytesToString(splitTextToWords([]byte("Wie geht es Ihnen"))))

	expect(t, "je/ /suis/ /enchanté/ /de/ /cette/ /pièce/", bytesToString(splitTextToWords([]byte("Je suis enchanté de cette pièce"))))
}
func TestTokenIDInTrie(t *testing.T) {
	var seg Segmenter
	if err := seg.LoadDictionary([]string{"testdata/test_dict1.txt", "testdata/test_dict2.txt"}, nil); err != nil {
		t.Error(err)
	}
	id, _ := seg.dict.trie.Jump([]byte("兩"), 0)
	//預期得到token塞入的索引值
	expect(t, "3", seg.dict.GetIDKeyValue(id))

	id, _ = seg.dict.trie.Jump([]byte("台"), 0)
	expect(t, "0", seg.dict.GetIDKeyValue(id))
	id, _ = seg.dict.trie.Jump([]byte("台灣"), 0)
	expect(t, "10", seg.dict.GetIDKeyValue(id))
}

func TestTokenPrefixPredict(t *testing.T) {
	var seg Segmenter
	if err := seg.LoadDictionary([]string{"testdata/test_dict1.txt", "testdata/test_dict2.txt"}, nil); err != nil {
		t.Error(err)
	}

	ids := seg.dict.trie.PrefixPredict([]byte("台"), 0)
	for _, id := range ids {
		seg.dict.GetIDKeyValue(id)
	}

	expect(t, "7", len(ids))
}

func TestTokenPrefixMatch(t *testing.T) {
	var seg Segmenter
	if err := seg.LoadDictionary([]string{"testdata/test_dict1.txt", "testdata/test_dict2.txt"}, nil); err != nil {
		t.Error(err)
	}

	ids := seg.dict.trie.PrefixMatch([]byte("台積電"), 0)
	for _, id := range ids {
		seg.dict.GetIDKeyValue(id)
	}
	expect(t, "2", len(ids))
}

func TestSegment(t *testing.T) {
	// declare seg as Segmenter
	var seg Segmenter
	if err := seg.LoadDictionary([]string{"testdata/test_dict1.txt", "testdata/test_dict2.txt"}, nil); err != nil {
		t.Error(err)
	}

	expect(t, "21", seg.dict.NumTokens()) //使用在seg上的字典token個數

	segments := seg.Segment([]byte("台灣有兩千三百萬人口"), false)

	segStr := SegmentsToString(segments, false)
	fmt.Println(segStr)

	expect(t, "台灣/ 有/p3 兩千/p14 三百萬/ 人口/p16 ", segStr)
	expect(t, "5", len(segments))
	expect(t, "0", segments[0].start)
	expect(t, "6", segments[0].end)
	expect(t, "6", segments[1].start)
	expect(t, "9", segments[1].end)
	expect(t, "9", segments[2].start)
	expect(t, "15", segments[2].end)
	expect(t, "15", segments[3].start)
	expect(t, "24", segments[3].end)
	expect(t, "24", segments[4].start)
	expect(t, "30", segments[4].end)
}

func TestLargeDictionary(t *testing.T) {
	dictItems := make([]DictItem, 0)
	dictItems = append(dictItems, DictItem{Text: "錢都", FreqText: "100", Pos: "n"})
	dictItems = append(dictItems, DictItem{Text: "吉野家", FreqText: "100", Pos: "n"})
	dictItems = append(dictItems, DictItem{Text: "呷尚寶", FreqText: "100", Pos: "n"})
	dictItems = append(dictItems, DictItem{Text: "麥味登", FreqText: "100", Pos: "n"})
	dictItems = append(dictItems, DictItem{Text: "鼎泰豐", FreqText: "100", Pos: "n"})
	dictItems = append(dictItems, DictItem{Text: "福圓號", FreqText: "100", Pos: "n"})
	dictItems = append(dictItems, DictItem{Text: "Subway", FreqText: "100", Pos: "n"})
	dictItems = append(dictItems, DictItem{Text: "派克雞排", FreqText: "100", Pos: "n"})
	dictItems = append(dictItems, DictItem{Text: "池上飯包", FreqText: "100", Pos: "n"})
	dictItems = append(dictItems, DictItem{Text: "怡客咖啡", FreqText: "100", Pos: "n"})
	if err := prodSeg.LoadDictionary([]string{"server/data/dictionary.txt"}, dictItems); err != nil {
		t.Error(err)
	}

	s := prodSeg.Segment([]byte("大蝦"), true)
	expect(t, "大/a 蝦/n ", SegmentsToString(s, true))

	segs := prodSeg.Segment([]byte("池上飯包"), true)
	expect(t, "池上/s 飯/n 包/v ", SegmentsToString(segs, true))

	seg := prodSeg.Segment([]byte("甜玉米"), true)
	expect(t, "甜/a 玉米/nr ", SegmentsToString(seg, false))

	segf := prodSeg.Segment([]byte("健康飲食的諮詢平台"), false)
	expect(t, "健康/a 飲食/n 的/uj 諮詢/vn 平台/n ", SegmentsToString(segf, false))
	expect(t, "健康/a 飲食/n 的/uj 諮詢/vn 平台/n ", SegmentsToString(segf, true))
	segt := prodSeg.Segment([]byte("健康飲食的諮詢平台"), true)
	expect(t, "健康/a 飲食/n 的/uj 諮詢/vn 平台/n ", SegmentsToString(segt, false))
	expect(t, "健康/a 飲食/n 的/uj 諮詢/vn 平台/n ", SegmentsToString(segt, true))

	segf = prodSeg.Segment([]byte("台灣人口"), false)
	expect(t, "台灣/nz 人口/n ", SegmentsToString(segf, false))
	expect(t, "台灣/nz 人口/n ", SegmentsToString(segf, true))

	segt = prodSeg.Segment([]byte("台灣人口"), true)
	expect(t, "台灣/nz 人口/n ", SegmentsToString(segt, false))
	expect(t, "台灣/nz 人口/n ", SegmentsToString(segt, true))

	segf = prodSeg.Segment([]byte("台灣人口有兩千三百萬"), false)
	expect(t, "台灣/nz 人口/n 有/v 兩千/m 三百萬/m ", SegmentsToString(segf, false))
	expect(t, "台灣/nz 人口/n 有/v 兩千/m 三/m 百萬/m 三百萬/m ", SegmentsToString(segf, true))

	segt = prodSeg.Segment([]byte("台灣人口有兩千三百萬"), true)
	expect(t, "台灣/nz 人口/n 有/v 兩千/m 三百萬/m ", SegmentsToString(segt, false))
	expect(t, "台灣/nz 人口/n 有/v 兩千/m 三/m 百萬/m 三百萬/m ", SegmentsToString(segt, true))
}

func TestMergeDictionaries(t *testing.T) {
	dictionaries := NewDictionaries()
	if err := dictionaries.MergeDictionary("server/data/dictionary.txt", "server/data/sc_dictionary.txt", "server/data/merge_dictionary.txt"); err != nil {
		t.Error(err)
	}
}
