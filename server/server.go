/*
JSON格式RPC服務
Input:
	POST/GET text
Output:
	{
		segments:[
			{"text":"伺服器", "pos":"n"},
			{"text":"指令", "pos":"n"}
		]
	}
*/

package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"path"
	"runtime"

	"git.championtek.com.tw/go/sego"
)

var (
	host         = flag.String("host", "", "HTTP伺服器名稱")
	port         = flag.Int("port", 8080, "HTTP伺服器端口")
	dict         = flag.String("dict", "../data/dictionary.txt", "字典文件")
	staticFolder = flag.String("static_folder", "static", "靜態頁面目錄")
	segmenter    = sego.Segmenter{}
)

type JsonResponse struct {
	Segments []*Segment `json:"segments"`
}

type Segment struct {
	Text string `json:"text"`
	Pos  string `json:"pos"`
}

func JsonRpcServer(w http.ResponseWriter, req *http.Request) {
	text := req.URL.Query().Get("text")
	if text == "" {
		text = req.PostFormValue("text")
	}

	//segment
	segments := segmenter.Segment([]byte(text), false)

	//output
	ss := []*Segment{}
	for _, segment := range segments {
		ss = append(ss, &Segment{Text: segment.Token().Text(), Pos: segment.Token().Pos()})
	}
	response, _ := json.Marshal(&JsonResponse{Segments: ss})

	w.Header().Set("Content-Type", "application/json")
	io.WriteString(w, string(response))
}

func main() {
	flag.Parse()

	runtime.GOMAXPROCS(runtime.NumCPU())

	_, currentFilePath, _, _ := runtime.Caller(0)
	dictFilePath := path.Join(path.Dir(currentFilePath), "data/dictionary.txt")
	fmt.Println("config file path: ", dictFilePath)

	segmenter.LoadDictionary([]string{dictFilePath}, nil)

	http.HandleFunc("/json", JsonRpcServer)
	http.Handle("/", http.FileServer(http.Dir(*staticFolder)))
	log.Print("Server start...")
	addr := fmt.Sprintf("%s:%d", *host, *port)
	if err := http.ListenAndServe(addr, nil); err != nil {
		log.Fatalln("start server error: ", err)
	}
}
