package sego

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
	"unicode"
	"unicode/utf8"
)

const (
	minTokenFrequency = 2 //僅從詞典文件內讀取 >= 此頻率的單詞
)

// Segmenter 分詞器結構，keep字典檔物件
type Segmenter struct {
	dict *Dictionary
}

//jumper 用於記錄 Viterbi 演算法中某字元處的向前單詞跳轉信息
type jumper struct {
	minDistance float32
	token       *Token
}

//Dictionary 回傳分詞器使用的辭典
func (seg *Segmenter) Dictionary() *Dictionary {
	return seg.dict
}

//LoadDictionary 載入詞典檔案，可以載入多個詞典，辭典用comma隔開，排在越前面的辭典優先載入單詞，
//當一個出現的單詞出現在多個載入詞典時，優先使用排在最前面的辭典; 另外新增alternates詞典字串陣列資料，新加入的辭典資料不需要先存為檔案
//@params files 詞典檔檔案
//@params dictItems 詞典檔字串
//詞典格式 : 單詞 頻率 詞性
//請注意單詞不允許空白存在
func (seg *Segmenter) LoadDictionary(files []string, dictItems []DictItem) error {
	seg.dict = NewDictionary()

	////sort dictItems array if dictItems is not null
	//itemsMap := make(map[int][]DictItem)
	//if dictItems != nil {
	//	for _, item := range dictItems {
	//		//fmt.Println(len(item.Text))
	//		itemsMap[len(item.Text)] = append(itemsMap[len(item.Text)], item)
	//	}
	//}

	for _, file := range files {
		log.Printf("Loading dictionary %s ...", file)
		dictFile, err := os.Open(file)
		if err != nil {
			errStr := fmt.Errorf("can't load dictionary %s with error %v", file, err)
			log.Println(errStr)
			return errStr
		}
		defer dictFile.Close()

		reader := bufio.NewReader(dictFile)

		var text string
		var freqText string
		var frequency int
		var pos string

		// Read token line by line for loaded dictionary
		// 開始建立辭典架構，包含tokens節點以及父子節點之間關係
		for {
			size, _ := fmt.Fscanln(reader, &text, &freqText, &pos)
			if size == 0 { // 沒有單詞存在，類似EOF
				break
			} else if size < 2 { // 僅有單詞(text)，不是我們要的
				continue
			} else if size == 2 { // 僅有單詞(text)以及頻率(freqText)
				// 沒有詞性標注時補上空字符串
				pos = ""
			}
			//轉換詞頻字串為整數型態
			var err error
			frequency, err = strconv.Atoi(freqText)
			if err != nil {
				continue
			}

			// 過濾頻率太小的詞，預設值為2
			if frequency < minTokenFrequency {
				continue
			}

			// 將單詞添加到分詞器的字典中
			if dictItems != nil {

			}

			words := splitTextToWords([]byte(text)) //逐行讀取載入的字典檔內容，並將單詞輸入
			token := Token{text: words, frequency: frequency, pos: pos}
			seg.dict.addToken(token)

			//確認新增的字典字詞也是按照其字詞長度規則加入
			//items, ok := itemsMap[len(text)]
			//if ok {
			//	for _, item := range items {
			//		log.Println("Loading dictionary from dictItems ...")
			//		frequency, err := strconv.Atoi(item.FreqText)
			//		if err != nil {
			//			continue
			//		}
			//		words := splitTextToWords([]byte(item.Text))
			//		token := Token{text: words, frequency: frequency, pos: item.Pos}
			//		seg.dict.addToken(token)
			//	}
			//
			//	delete(itemsMap, len(text))
			//}
		}
	}
	if dictItems != nil {
		for _, dictItem := range dictItems {
			log.Println("Loading dictionary from dictItems ...")
			frequency, err := strconv.Atoi(dictItem.FreqText)
			if err != nil {
				continue
			}
			words := splitTextToWords([]byte(dictItem.Text))
			token := Token{text: words, frequency: frequency, pos: dictItem.Pos}
			seg.dict.addToken(token)
		}
	}

	// 計算字典檔內每個單詞的路徑值(distance)，路徑值請參考Token結構註釋
	// 在於計算此token在辭典的重要性
	logTotalFrequency := float32(math.Log2(float64(seg.dict.totalFrequency)))
	for i := range seg.dict.tokens {
		token := &seg.dict.tokens[i]
		// 當詞頻(frequency)越大，則路徑越短，重要性則越高
		token.distance = logTotalFrequency - float32(math.Log2(float64(token.frequency)))
	}

	// 對每個token(單詞)進行細緻劃分，用於搜尋引擎模式
	for i := range seg.dict.tokens {
		token := &seg.dict.tokens[i]
		segments := seg.segmentWords(token.text, true)

		// 計算要加入的子單詞數目
		numTokenToAdd := 0
		for iToken := 0; iToken < len(segments); iToken++ {
			if len(segments[iToken].token.text) > 0 {
				numTokenToAdd++
			}
		}
		token.segments = make([]*Segment, numTokenToAdd)

		// 加入子單詞
		iSegmentsToAdd := 0
		for iToken := 0; iToken < len(segments); iToken++ {
			if len(segments[iToken].token.text) > 0 {
				token.segments[iSegmentsToAdd] = &segments[iToken]
				iSegmentsToAdd++
			}
		}
	}

	log.Println("Dictionary loading finish!!")
	return nil
}

//Segment 對文本分詞
//Input: bytes UTF8文本的字節數組
//Output: []Segment 劃分的分詞
func (seg *Segmenter) Segment(bytes []byte, searchMode bool) []Segment {
	return seg.internalSegment(bytes, searchMode)
}

//InternalSegment 取得切分區段
func (seg *Segmenter) InternalSegment(b []byte, searchMode bool) []Segment {
	return seg.internalSegment(b, searchMode)
}

//internalSegment
func (seg *Segmenter) internalSegment(b []byte, searchMode bool) []Segment {
	//處理特殊情況
	if len(b) == 0 {
		//回傳空區段陣列
		return []Segment{}
	}

	//劃分字元
	text := splitTextToWords(b)

	////劃分單字，例如 全_家_肉_包 經過劃分後變成 全家_肉包
	//segs := seg.segmentWords(text, searchMode)
	//for _, v := range segs {
	//	fmt.Println(v.Token().Text())
	//}
	return seg.segmentWords(text, searchMode)
}

func (seg *Segmenter) segmentWords(text []Text, searchMode bool) []Segment {
	//搜索模式下該單詞已無繼續切分的可能性
	if searchMode && len(text) == 1 {
		//單字無需分段，回傳空區段陣列
		return []Segment{}
	}

	//jumpers定義每個字元處的向前跳轉訊息，包含這個跳轉對應的分詞，以及從文本段開始到該字元的最短路徑值
	jumpers := make([]jumper, len(text))
	tokens := make([]*Token, seg.dict.maxTokenLength)
	for current := 0; current < len(text); current++ {
		//找到前一個字元處的最短路徑，以便計算後續路徑值
		var baseDistance float32
		if current == 0 {
			//當本字元在文本首部時，基礎距離應該是零
			baseDistance = 0
		} else {
			//上一個jumper的最短距離
			baseDistance = jumpers[current-1].minDistance
		}

		//尋找所有以當前字元開頭的單詞，例如以"台"開頭的單詞，台灣/台北/台南/台場/台胞/台客/台上
		numTokens := seg.dict.lookupTokens(text[current:minInt(current+seg.dict.maxTokenLength, len(text))], tokens)
		//對所有可能的的單詞，更新單詞結束字元處的跳轉信息
		for iToken := 0; iToken < numTokens; iToken++ {
			//log.Printf("the length of the token[%d].text is %d", iToken, len(tokens[iToken].text))
			location := current + len(tokens[iToken].text) - 1
			if !searchMode || current != 0 || location != len(text)-1 {
				updateJumper(&jumpers[location], baseDistance, tokens[iToken])
			}
		}

		// 當前字元沒有對應單詞時，補加一個偽單詞(fake)
		if numTokens == 0 || len(tokens[0].text) > 1 {
			updateJumper(&jumpers[current], baseDistance, &Token{text: []Text{text[current]}, frequency: 1, distance: 32, pos: "x"})
		}
	}

	//從後向前掃描第一遍得到需要添加的單詞數目
	numSeg := 0
	for index := len(text) - 1; index >= 0; {
		location := index - len(jumpers[index].token.text) + 1
		numSeg++
		index = location - 1
	}

	//從後向前掃瞄第二遍添加單詞之最終結果
	outputSegments := make([]Segment, numSeg)
	for index := len(text) - 1; index >= 0; {
		location := index - len(jumpers[index].token.text) + 1
		numSeg--
		outputSegments[numSeg].token = jumpers[index].token
		index = location - 1
	}

	//計算各單詞的字節位置
	bytePosition := 0
	for iSeg := 0; iSeg < len(outputSegments); iSeg++ {
		outputSegments[iSeg].start = bytePosition
		bytePosition += textSliceByteLength(outputSegments[iSeg].token.text)
		outputSegments[iSeg].end = bytePosition
	}

	return outputSegments
}

//updateJumper 更新跳轉信息:
// 1. 當該位置未被訪問過時(jumper.minDistance=0的狀況) or
// 2. 當該位置的當前最短位置大於新的最短路徑時
// 將當前位置的最短路徑更新為baseDistance+新單詞的概率
func updateJumper(jumper *jumper, baseDistance float32, token *Token) {
	newDistance := baseDistance + token.distance
	if jumper.minDistance == 0 || jumper.minDistance > newDistance {
		jumper.minDistance = newDistance
		jumper.token = token
	}
}

//splitTextToWords 將單詞/句子/文章切成字元(資料型態為byte陣列)
func splitTextToWords(text Text) []Text {
	output := make([]Text, 0, len(text)/3) // create a slice and specify a capacity which is len(text)/3，因為text轉成byte後長度變為3
	//log.Printf("The capacity of output is %d", len(text)/3)
	current := 0
	isAlphaNumeric := true
	alphaNumberStart := 0
	for current < len(text) {
		r, size := utf8.DecodeRune(text[current:]) //轉成rune型態，也就是uint32型態的值，以字符方式來看待; r:字符 size:字節長度(byte[]長度)
		//log.Printf("rune : %s", string(r))
		if size <= 2 && (unicode.IsLetter(r) || unicode.IsNumber(r)) {
			//目前讀入的單詞是拉丁字母或是數字，非中日韓文字
			if !isAlphaNumeric {
				alphaNumberStart = current
				isAlphaNumeric = true
			}
		} else { // size > 2
			if isAlphaNumeric {
				isAlphaNumeric = false
				if current != 0 { //將之前讀到的拉丁字母或是數字加入output，例如2300萬，則先將2300加入output
					output = append(output, toLower(text[alphaNumberStart:current]))
				}
			}
			//在處理完上述拉丁字母或是數字後，再將目前讀到的單詞加入output
			output = append(output, text[current:current+size])
		}
		current += size
	}

	// 處理最後一個字元是英文的狀況
	if isAlphaNumeric {
		if current != 0 {
			output = append(output, toLower(text[alphaNumberStart:current]))
		}
	}

	return output
}

//minInt 取兩數最小值
func minInt(a, b int) int {
	if a > b {
		return b
	}
	return a
}

//maxInt 取兩數最大值
func maxInt(a, b int) int {
	if a > b {
		return a
	}
	return b
}

//toLower 將英文詞轉化成小寫
func toLower(text []byte) []byte {
	output := make([]byte, len(text))
	for i, t := range text {
		if t >= 'A' && t <= 'Z' {
			output[i] = t - 'A' + 'a'
		} else {
			output[i] = t
		}
	}
	return output
}
