package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"runtime"
	"time"

	"git.championtek.com.tw/go/sego"
)

var (
	segmenter  = sego.Segmenter{}
	numThreads = runtime.NumCPU()
	task       = make(chan []byte, numThreads*40)
	done       = make(chan bool, numThreads)
	numOfRuns  = 50
)

func worker() {
	for line := range task {
		segmenter.Segment(line, false)
	}
	done <- true
}

func goroutines() {
	runtime.GOMAXPROCS(numThreads)

	segmenter.LoadDictionary("../data/dictionary.txt")

	file, err := os.Open("../testdata/bailuyuan.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	size := 0
	lines := [][]byte{}
	for scanner.Scan() {
		var text string
		fmt.Sscanf(scanner.Text(), "%s", &text)
		content := []byte(text)
		size += len(content)
		lines = append(lines, content)
	}

	for i := 0; i < numThreads; i++ {
		go worker()
	}
	log.Print("segment begin...")

	t0 := time.Now()

	for i := 0; i < numOfRuns; i++ {
		for _, l := range lines {
			task <- l
		}
	}
	close(task)

	for i := 0; i < numThreads; i++ {
		<-done
	}

	t1 := time.Now()
	log.Printf("segment cost %v", t1.Sub(t0))
	log.Printf("segment %f MB/s", float64(size*numOfRuns)/t1.Sub(t0).Seconds()/(1024*1024))
}
