package main

/**
測試 sego 分詞速度

go run benchmark.go

## 輸出文件:
go run benchmark.go --output=output.txt

## 分析效能:
go build benchmark.go
./benchmark --cpuprofile=cpu.prof
go tool pprof benchmark cpu.prof

## 分析記憶體使用
go build benchmark.go
./benchmark --memprofile=mem.prof
go tool pprof benchmark mem.prof
*/

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"
	"runtime/pprof"
	"time"

	"git.championtek.com.tw/go/sego"
)

var (
	cpuprofile = flag.String("cpuprofile", "", "cpu profile document")
	memprofile = flag.String("memprofile", "", "memory profile document")
	output     = flag.String("output", "", "export benchmark result document")
	numRuns    = 20
)

func benchmark() {
	//specify single thread
	runtime.GOMAXPROCS(1)

	flag.Parse()

	//record time
	t0 := time.Now()

	var segmenter sego.Segmenter
	segmenter.LoadDictionary("../data/dictionary.txt")

	//record time
	t1 := time.Now()

	log.Printf("Dictionary loading cost %v", t1.Sub(t0))

	//write into memory
	if *memprofile != "" {
		f, err := os.Create(*memprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.WriteHeapProfile(f)
		defer f.Close()
	}

	//Open document to segment
	file, err := os.Open("../testdata/bailuyuan.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	//scan line by line
	scanner := bufio.NewScanner(file)
	size := 0
	lines := [][]byte{}
	for scanner.Scan() {
		var text string
		fmt.Sscanf(scanner.Text(), "%s", &text)
		content := []byte(text)
		size += len(content)
		lines = append(lines, content)
	}

	//specify the output file
	var of *os.File
	if *output != "" {
		of, err := os.Create(*output)
		if err != nil {
			log.Fatal(err)
		}
		defer of.Close()
	}

	t2 := time.Now()

	if *cpuprofile != "" {
		cf, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(cf)
		// defer pprof.StopCPUProfile()
	}

	//segment
	for i := 0; i < numRuns; i++ {
		for _, l := range lines {
			segments := segmenter.Segment(l, false)
			if *output != "" {
				of.WriteString(sego.SegmentsToString(segments, false))
				of.WriteString("\n")
			}
		}
	}

	//stop cpu profile
	if *cpuprofile != "" {
		defer pprof.StopCPUProfile()
	}

	t3 := time.Now()
	log.Printf("segment cost %v", t3.Sub(t2))
	log.Printf("segment %f MB/s", float64(size*numRuns)/t3.Sub(t2).Seconds()/(1024*1024))
}
