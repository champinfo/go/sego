package main

import (
	"flag"
	"fmt"

	"git.championtek.com.tw/go/sego"
)

var (
	text = flag.String("text", "Google發布消息，停止軟體授權給華為Android手機", "要分詞的內文")
)

func main() {
	flag.Parse()

	var seg sego.Segmenter
	seg.LoadDictionary("../data/dictionary.txt")

	segments := seg.Segment([]byte(*text), false)
	fmt.Println(sego.SegmentsToString(segments, true))
}
